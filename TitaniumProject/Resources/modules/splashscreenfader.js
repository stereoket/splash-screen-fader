var anStr;
var orientation = 'port';
var fireEvent;
var loadBuffer;
var device;
var winFader;
var mainWindow = 'mainWindow.jpg';
Ti.Gesture.addEventListener('orientationchange', function(e) {
	Ti.API.info('Orientation: ' + e.orientation);
	try {
		Ti.API.info('Portrait:' + e.source.isPortrait());
		if(e.source.isPortrait()) {
			orientation = 'port';
		} else {
			orientation = 'land';
		}
	} catch (E) {
		Ti.API.debug(E.message);
	}
	Ti.API.info(Ti.Gesture.orientation);
	Ti.API.info('Value of the event flag: ' + fireEvent);
	if(fireEvent) {
		Ti.App.fireEvent(fireEvent, {
			orientation : orientation
		});
	}
	return;
});
/**
 * Returns the value of the orientation from the attached Gesture listener
 * @return (string) in value 'land' OR 'port'
 */
exports.returnOrientation = function() {
	return orientation;
}
exports.deviceProperties = function() {
	var device = {
		os : Ti.Platform.osname,
		density : Ti.Platform.displayCaps.density,
		platformHeight : Ti.Platform.displayCaps.platformHeight,
		platformWidth : Ti.Platform.displayCaps.platformWidth,
		dpi : Ti.Platform.displayCaps.dpi
	};
	return device;
}

exports.setBackgroundColor = function(bgColor) {
	if(bgColor !== undefined) {
		if(device.os !== 'android')
			winFader.backgroundColor = bgColor;
		Ti.UI.setBackgroundColor(bgColor);
	} else {
		if(device.os !== 'android') {
			winFader.backgroundColor = '#000';
		}
		Ti.UI.setBackgroundColor('#000');
		
	}
}

exports.animateSplash = function(args) {
	device = exports.deviceProperties();
	// Set background colour properties for windows

	if(device.os !== 'android') {
		//
		// Create a window to hold the splash screen image
		//
		winFader = Titanium.UI.createWindow({
			navBarHidden : true,
			exitOnClose : true // for android (non tab group)
		});
		
		
		if(args.imageBackground !== undefined && args.imageBackground ){
			backgroundImageView = Ti.UI.createImageView({
			image: '/images/' + mainWindow,
			bottom: 0
		});
			winFader.add(backgroundImageView);
		}
		
	}
	exports.setBackgroundColor(args.backgroundColor);
	if(device.os === 'android') {
		Ti.App.fireEvent('closeSplash');
		return;
	}

	// Locks window to portrait, allowing the image to fade and not change.
	winFader.orientationModes = [Ti.UI.PORTRAIT];
	/**
	 * Fire off the gesture event listener to determine the orientation, this will not work until app is loaded in memory.
	 */
	Ti.Gesture.fireEvent('orientationchange');
	fireEvent = (args.fireEvent !== undefined) ? args.fireEvent : false;
	loadBuffer = (args.loadBuffer !== undefined) ? args.loadBuffer : 5;
	/**
	 * Main splash loader wrapped in timer to allow initial gesture / orientation to be recognised.
	 */
	setTimeout(function() {
		var splashImageName = (args.splashImage !== undefined) ? args.splashImage : 'splashImage';
		var androidPixelValue = 50;
		// Status Bar & AppName Title Bar display pixel height
		var fadeoutTime = (args.fadeLength !== undefined) ? args.fadeLength : 3500;
		// Fade time default is 3.5 seconds

		var imgName = splashImageName + '-' + device.os;
		// iPad Orientation name set
		imgName = (device.os === 'ipad') ? imgName + '-' + orientation : imgName;
		imgName = imgName + '.png';

		// setting it so that default image can be reused.
		var dpi = 160;
		// set DPI according to
		switch(device.density) {
			case 'high':
				dpi = 240;
				anStr = 'hdpi';
				break;
			case 'medium':
				dpi = 160;
				anStr = 'mdpi';
				break;
			case 'low':
				dpi = 120;
				anStr = 'ldpi';
				break;
		}

		if(device.os === 'android') {

			// check for HVGA DEVICEs
			if(device.platformHeight >= 480 && device.platformWidth >= 270 && device.platformWidth <= 360) {
				// not including the 640x240 device yet
				Ti.API.info('HVGA DEVICE');
				imgName = splashImageName + '-android-notlong-' + orientation + '-' + anStr + '.png';

			}
			androidHeightOffset = androidPixelValue * (dpi / 160);
			androidHeightOffset = androidHeightOffset - (androidHeightOffset * 2);
		}
		iOSHeightOffset = (device.os === 'iphone') ? -10 : -10;
		var theLogo = Ti.UI.createImageView({
			top : (device.os === 'android') ? androidHeightOffset : iOSHeightOffset, // readjust positioning for iPhone
			left : 0,
			height : device.platformHeight,
			width : device.platformWidth,
			image : '/images/' + imgName
		});
		// Build animation for fade out
		var t = Ti.UI.create2DMatrix().scale(1);
		var af1 = Ti.UI.createAnimation({
			opacity : 0,
			transform : t,
			duration : fadeoutTime
		});
		if(device.os !== 'android')
			af1.curve = Ti.UI.ANIMATION_CURVE_EASE_OUT;
		af1.addEventListener('complete', function() {
			Ti.App.fireEvent('closeSplash');
		});
		// Add objects to window and apply animation
		Ti.API.info('adding images/' + imgName + ' adjusting image by ' + theLogo.top);
		winFader.add(theLogo);
		winFader.open({
			animated : false
		});
		theLogo.animate(af1);
	}, loadBuffer);
}