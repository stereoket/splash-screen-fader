//
// create base root window - exit on close property added for android
//
var win = Titanium.UI.createWindow({
	title : 'Application Home',
	backgroundColor : '#fff',
	exitOnClose: true,
	navBarHidden: false
});
win.orientationModes = [Ti.UI.PORTRAIT, Ti.UI.LANDSCAPE_LEFT];
var label1 = Titanium.UI.createLabel({
	color : '#000',
	text : 'Main Application Window, after fade.',
	font : {
		fontSize : 20,
		fontFamily : 'Helvetica Neue'
	},
	textAlign : 'center',  
	width : 'auto'
});

win.add(label1);

//
// SplashScreen Fader for iOS & Android START
//
	Ti.App.addEventListener('closeSplash', function() {
		win.animate({
			opacity : 1,
			backgroundColor : '#fff'
		});
		win.open();
	});
	Ti.App.addEventListener('deviceOrientation', function(e) {
		// Your custom code to trigger when orientation fires
		Ti.API.info(e);
	});
	var Fader = require('/modules/splashscreenfader');
	var device = Fader.deviceProperties();
	if(device.os !== 'android') win.opacity = 0;
	Fader.animateSplash({
		fadeLength : 7000,
		backgroundColor : '#1A2848',
		imageBackground: true,
		fireEvent: 'deviceOrientation'
	});


